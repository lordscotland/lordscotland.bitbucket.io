#!/usr/bin/perl

use strict;
use HTML::Entities;
use URI::Escape;

my $outfn='index.htm';
open(my $fh,'>',$outfn);
print $fh '<HTML><HEAD><TITLE>Index</TITLE></HEAD><BODY><h1>Index</h1><hr/><ul>';
while(<*.htm>){
  next if $_ eq $outfn;
  print $fh '<li><a href="'.uri_escape($_).'">'.encode_entities($_).'</a></li>';
}
print $fh '</ul></BODY></HTML>';
close($fh);
